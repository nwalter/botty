package de.mcripper.chatlib;

/**
 * Represents a user in the system.
 * 
 * @author Niklas Walter
 * 
 */
public interface User {

	public String getName();
}
