package de.mcripper.chatlib;

public class ChatLibRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 8370195068143677716L;

	public ChatLibRuntimeException(final Throwable cause) {
		super(cause);
	}

	public static void pack(final Throwable cause) {
		throw new ChatLibRuntimeException(cause);
	}

}
