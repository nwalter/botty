package de.mcripper.chatlib;

/**
 * Represents a factory for creating connections.
 * 
 * @author Niklas Walter
 * 
 */
public interface ConnectionFactory {

	public Connection create();

}
