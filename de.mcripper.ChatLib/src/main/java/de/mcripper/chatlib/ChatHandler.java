/**
 * 
 */
package de.mcripper.chatlib;

/**
 * @author Niklas Walter
 * 
 */
public interface ChatHandler {

	public void chatCreated(Chat chat);
}
