/**
 * 
 */
package de.mcripper.chatlib;

/**
 * Represents a connection to a specific server.
 * 
 * @author Niklas Walter
 * 
 */
public interface Connection {

	public void connect();

	public void disconnect();

	public Chat createChat(User user);

	public User getUser();

	public void addChatHandler(ChatHandler handler);

	public void removeChatHandler(ChatHandler handler);
}
