package de.mcripper.chatlib;

import com.google.inject.Inject;

/**
 * Represents a message in a chat.
 * 
 * @author Niklas Walter
 * 
 */
public class Message {

	private final String content;
	private final Chat chat;
	private final User sender;

	@Inject
	public Message(final String content, final Chat chat, final User sender) {
		super();
		this.content = content;
		this.chat = chat;
		this.sender = sender;
	}

	/**
	 * Returns the content of the message.
	 */
	public String getContent() {
		return this.content;
	}

	public Chat getChat() {
		return this.chat;
	}

	public User getSender() {
		return this.sender;
	}
}
