package de.mcripper.chatlib;

import com.google.inject.Injector;

public class GuiceConnectionFactory implements ConnectionFactory {

	private final Injector injector;

	public GuiceConnectionFactory(final Injector injector) {
		this.injector = injector;
	}

	@Override
	public Connection create() {
		return this.injector.getInstance(Connection.class);
	}

}
