package de.mcripper.chatlib;

import java.util.Collection;

public abstract class UserManager {

	public abstract Collection<User> getUsers(final String... participant);

	public abstract User getUser(final String participant);

}
