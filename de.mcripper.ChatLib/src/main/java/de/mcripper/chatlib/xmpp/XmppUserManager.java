package de.mcripper.chatlib.xmpp;

import java.util.Collection;

import com.google.common.collect.Lists;

import de.mcripper.chatlib.User;
import de.mcripper.chatlib.UserManager;

public class XmppUserManager extends UserManager {

	@Override
	public Collection<User> getUsers(final String... participants) {
		final Collection<User> result = Lists.newArrayList();
		for (final String string : participants) {
			result.add(this.getUser(string));
		}
		return result;
	}

	@Override
	public User getUser(final String participant) {
		return new XmppUser(participant);
	}

}
