package de.mcripper.chatlib.xmpp;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;

import de.mcripper.chatlib.Connection;
import de.mcripper.chatlib.UserManager;

/**
 * Represents the Google Guice configuration for xmpp chat.
 * 
 * @author Niklas Walter
 * 
 */
public class XmppModule extends AbstractModule {

	private final String user;
	private final String password;
	private final String server;
	private final Integer port;

	public XmppModule(final String user, final String password,
			final String server, final Integer port) {
		super();
		this.user = user;
		this.password = password;
		this.server = server;
		this.port = port;
	}

	@Override
	protected void configure() {
		this.bind(String.class).annotatedWith(Names.named("user"))
				.toInstance(this.user);
		this.bind(String.class).annotatedWith(Names.named("password"))
				.toInstance(this.password);
		this.bind(String.class).annotatedWith(Names.named("server"))
				.toInstance(this.server);
		this.bind(Integer.class).annotatedWith(Names.named("port"))
				.toInstance(this.port);

		this.bind(Connection.class).to(XmppConnection.class)
				.in(Singleton.class);
		this.bind(UserManager.class).to(XmppUserManager.class)
				.in(Singleton.class);
	}
}
