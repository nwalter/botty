package de.mcripper.chatlib.xmpp;

import org.jivesoftware.smack.util.StringUtils;

import de.mcripper.chatlib.User;

public class XmppUser implements User {

	private final String userID;

	public XmppUser(final String userID) {
		this.userID = userID;
	}

	@Override
	public String getName() {
		return StringUtils.parseName(this.userID);
	}

	public String getUserID() {
		return this.userID;
	}

}
