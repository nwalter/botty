package de.mcripper.chatlib.xmpp;

import java.util.Collection;

import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

import de.mcripper.chatlib.Chat;
import de.mcripper.chatlib.ChatHandler;
import de.mcripper.chatlib.ChatLibRuntimeException;
import de.mcripper.chatlib.Connection;
import de.mcripper.chatlib.User;

public class XmppConnection implements Connection {

	private final Logger logger = LoggerFactory.getLogger(XmppConnection.class);

	@Inject
	@Named("user")
	private String username;

	@Inject
	@Named("password")
	private String password;
	private final XMPPConnection connection;
	private final Collection<ChatHandler> chatHandler;
	@Inject
	private Injector injector;

	private XmppUser user;

	@Inject
	public XmppConnection(@Named("server") final String host,
			@Named("port") final Integer port) {
		super();

		this.chatHandler = Sets.newHashSet();

		this.connection = new XMPPConnection(new ConnectionConfiguration(host,
				port));
		this.connection.getChatManager().addChatListener(
				new ChatManagerListener() {

					@Override
					public void chatCreated(
							final org.jivesoftware.smack.Chat chat,
							final boolean createdLocally) {
						if (!createdLocally) {

							XmppConnection.this.logger.debug("Chat created");
							XmppConnection.this.notifyChatHanlder(chat);
						}
					}
				});
	}

	@Override
	public void connect() {
		this.user = new XmppUser(this.username);
		try {
			this.logger.info("Connecting to server");
			this.connection.connect();
			this.logger.info("Logging in as {}", this.user);
			this.connection.login(this.username, this.password);
		} catch (final XMPPException e) {
			ChatLibRuntimeException.pack(e);
		}
	}

	@Override
	public void disconnect() {
		this.logger.info("Disconnecting from server");
		this.connection.disconnect();
	}

	@Override
	public Chat createChat(final User user) {
		assert user instanceof XmppUser;
		final XmppUser xmppUser = (XmppUser) user;
		final NoopMessageListener noopMessageListener = new NoopMessageListener();
		final org.jivesoftware.smack.Chat chat = this.connection
				.getChatManager().createChat(xmppUser.getUserID(),
						noopMessageListener);
		final XmppChat xmppChat = new XmppChat(chat, this);
		chat.removeMessageListener(noopMessageListener);
		this.injector.injectMembers(xmppChat);

		return xmppChat;
	}

	@Override
	public User getUser() {
		return this.user;
	}

	@Override
	public void addChatHandler(final ChatHandler handler) {
		this.chatHandler.add(handler);
	}

	@Override
	public void removeChatHandler(final ChatHandler handler) {
		this.chatHandler.remove(this.chatHandler);
	}

	protected void notifyChatHanlder(final org.jivesoftware.smack.Chat chat) {
		final XmppChat xmppChat = new XmppChat(chat, this);
		this.injector.injectMembers(xmppChat);

		for (final ChatHandler handler : this.chatHandler) {
			handler.chatCreated(xmppChat);
		}
	}
}
