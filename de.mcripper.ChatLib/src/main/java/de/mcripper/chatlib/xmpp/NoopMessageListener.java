package de.mcripper.chatlib.xmpp;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

public class NoopMessageListener implements MessageListener {

	@Override
	public void processMessage(final Chat chat, final Message message) {
	}

}
