package de.mcripper.chatlib;

import java.util.Collection;

/**
 * Represents a chat with one or more participants.
 * 
 */
public interface Chat {

	/**
	 * Return the participants of the chat.
	 */
	public Collection<User> getParticipants();

	/**
	 * Sends a message to the chat.
	 */
	public void sendMessage(Message message);

	/**
	 * Registers a message handler, that will be notified of incoming messages.
	 */
	public void addMessageHandler(MessageHandler messageHandler);

	/**
	 * Removes a message handler from the chat.
	 */
	public void removeMessageHandler(MessageHandler messageHandler);

	public void sendMessage(String string);
}
