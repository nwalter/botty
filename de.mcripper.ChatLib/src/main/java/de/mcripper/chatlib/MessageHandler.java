package de.mcripper.chatlib;

public interface MessageHandler {

	public void receive(Chat chat, Message message);
}
