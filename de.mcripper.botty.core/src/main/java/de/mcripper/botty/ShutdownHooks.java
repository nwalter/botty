package de.mcripper.botty;

import java.util.List;

import com.google.common.collect.Lists;

public final class ShutdownHooks {

    private static ShutdownHooks instance;

    private final List<ShutdownHook> hooks;

    private final Thread shutdownThread = new Thread(new Runnable() {

        @Override
        public void run() {
            ShutdownHooks.this.executeHooks();
        }
    });

    private ShutdownHooks() {
        this.hooks = Lists.newArrayList();
        this.shutdownThread.setName("Shutdown Hook Executor");
        Runtime.getRuntime().addShutdownHook(this.shutdownThread);
    }

    private void executeHooks() {
        for (final ShutdownHook hook : this.hooks) {
            hook.onShutdown();
        }
    }

    public static void registerHook(final ShutdownHook shutdownHook) {
        if (instance == null) {
            instance = new ShutdownHooks();
        }
        instance.hooks.add(shutdownHook);
    }
}
