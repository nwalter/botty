package de.mcripper.botty;

import java.io.IOException;
import java.util.Properties;

public class PropertiesConfiguration implements BottyConfiguration {

    private static final String SETTINGS_PROPERTIES = "/settings.properties";

    private final Properties properties;

    public PropertiesConfiguration() {
        this.properties = new Properties();
        try {
            this.properties.load(PropertiesConfiguration.class.getResourceAsStream(SETTINGS_PROPERTIES));
        } catch (final IOException e) {
            throw new RuntimeException("Error while reading properties file", e);
        }
    }

    @Override
    public String getServer() {
        return this.properties.getProperty("server", "localhost");
    }

    @Override
    public String getPassword() {
        return this.properties.getProperty("password");
    }

    @Override
    public String getUser() {
        return this.properties.getProperty("user");
    }

    @Override
    public Integer getPort() {
        return Integer.parseInt(this.properties.getProperty("port", "5222"));
    }

    @Override
    public String getCommandPrefix() {
        return this.properties.getProperty("commandPrefix", "!");
    }

}
