package de.mcripper.botty;

import java.util.Map;

public abstract class BottyCommand {

    private Map<String, Object> values;

    protected <T> void store(final String identifier, final T value) {
        this.values.put(identifier, value);
    }

    @SuppressWarnings("unchecked")
    protected <T> T load(final String identifier) {
        return (T) this.values.get(identifier);
    }
}
