package de.mcripper.botty.commandParser;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.mcripper.chatlib.Message;

public class CommandInstance {

    private final Message original;
    private final String commandName;
    private final List<String> arguments;

    public CommandInstance(final Message message, final String cmd, final List<String> args) {
        this.original = message;
        this.commandName = cmd;
        this.arguments = ImmutableList.copyOf(args);
    }

    public Message getOriginal() {
        return this.original;
    }

    public String getCommandName() {
        return this.commandName;
    }

    public List<String> getArguments() {
        return this.arguments;
    }

}
