package de.mcripper.botty.commandParser;

@SuppressWarnings("serial")
public class ParserException extends Exception {

    public ParserException(final String message) {
        super(message);
    }

}
