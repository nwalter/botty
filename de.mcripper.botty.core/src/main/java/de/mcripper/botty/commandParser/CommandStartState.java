package de.mcripper.botty.commandParser;

import de.mcripper.botty.BottyConfiguration;
import de.mcripper.botty.commandParser.CommandParser.ParserState;

public class CommandStartState extends ParserState {

    private String read = "";
    private final String commandPrefix;

    public CommandStartState(final BottyConfiguration configuration, final CommandParser parser) {
        super(parser);
        this.commandPrefix = configuration.getCommandPrefix();
    }

    @Override
    protected void parse(final char currentChar, final CommandBuilder builder) throws ParserException {
        this.read += currentChar;
        if (this.commandPrefix.startsWith(this.read)) {
            if (this.commandPrefix.equals(this.read)) {
                this.setState(new CommandNameState(this.getParser()));
            }
        } else {
            throw new ParserException(String.format("Command has to start with %s", this.commandPrefix));
        }

    }
}
