package de.mcripper.botty.commandParser;

import de.mcripper.botty.BottyConfiguration;
import de.mcripper.chatlib.Message;

public class CommandParser {

    private ParserState state;

    public CommandParser(final BottyConfiguration configuration) {
        this.state = new CommandStartState(configuration, this);
    }

    public CommandInstance parse(final Message message) throws ParserException {
        final String content = message.getContent();
        final CommandBuilder builder = new CommandBuilder();
        this.parse(content, builder);
        return builder.build(message);

    }

    protected void parse(final String content, final CommandBuilder builder) throws ParserException {
        if (!content.isEmpty()) {
            this.state.parse(content.charAt(0), builder);
        }
        if (content.length() > 1) {
            this.parse(content.substring(1), builder);
        } else {
            this.state.parse(' ', builder);
            this.state.finish();
        }
    }

    protected abstract static class ParserState {

        private final CommandParser parser;

        protected abstract void parse(final char currentChar, CommandBuilder builder) throws ParserException;

        public ParserState(final CommandParser parser) {
            this.parser = parser;
        }

        protected void setState(final ParserState newState) {
            this.parser.state = newState;
        }

        protected CommandParser getParser() {
            return this.parser;
        }

        protected void finish() throws ParserException {
        }
    }
}
