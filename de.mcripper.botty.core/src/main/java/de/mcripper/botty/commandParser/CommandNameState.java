package de.mcripper.botty.commandParser;

import de.mcripper.botty.commandParser.CommandParser.ParserState;

public class CommandNameState extends ParserState {

    private String commandName = "";

    public CommandNameState(final CommandParser parser) {
        super(parser);
    }

    @Override
    protected void parse(final char currentChar, final CommandBuilder builder) throws ParserException {
        if (!Character.isWhitespace(currentChar)) {
            this.commandName += currentChar;
        }
        else {
            if (!this.commandName.isEmpty()) {
                builder.setCmd(this.commandName);
                this.setState(new NeutralState(this.getParser()));
            } else {
                throw new ParserException("No Command was provided!");
            }
        }
    }
}
