package de.mcripper.botty.commandParser;

import de.mcripper.botty.commandParser.CommandParser.ParserState;

public class NeutralState extends ParserState {

    public NeutralState(final CommandParser parser) {
        super(parser);
    }

    @Override
    protected void parse(final char currentChar, final CommandBuilder builder) throws ParserException {
        if (currentChar == '"' || currentChar == '\'') {
            this.setState(new EscapedArgumentState(this.getParser(), currentChar));
        } else if (!Character.isWhitespace(currentChar)) {
            this.setState(new ArgumentState(this.getParser(), currentChar));
        }
    }
}
