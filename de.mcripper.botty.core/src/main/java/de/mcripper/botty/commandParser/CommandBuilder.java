package de.mcripper.botty.commandParser;

import java.util.List;

import com.google.common.collect.Lists;

import de.mcripper.chatlib.Message;

public class CommandBuilder {

    private String cmd;
    private final List<String> args;

    public CommandBuilder() {
        this.args = Lists.newArrayList();
    }

    public void setCmd(final String cmd) {
        this.cmd = cmd;
    }

    protected String getCmd() {
        return this.cmd;
    }

    protected List<String> getArgs() {
        return this.args;
    }

    public void addArg(final String argument) {
        this.args.add(argument);
    }

    public CommandInstance build(final Message message) {
        return new CommandInstance(message, this.cmd, this.args);
    }
}
