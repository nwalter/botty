package de.mcripper.botty.commandParser;

import de.mcripper.botty.commandParser.CommandParser.ParserState;

public class EscapedArgumentState extends ParserState {

    private final char escapeChar;
    private String argument = "";

    public EscapedArgumentState(final CommandParser parser, final char escapeChar) {
        super(parser);
        this.escapeChar = escapeChar;
    }

    @Override
    protected void parse(final char currentChar, final CommandBuilder builder) throws ParserException {
        if (currentChar != this.escapeChar) {
            this.argument += currentChar;
        } else {
            builder.addArg(this.argument);
            this.setState(new NeutralState(this.getParser()));
        }
    }

    @Override
    protected void finish() throws ParserException {
        throw new ParserException(String.format("Argument %s is missing a closing %s", this.argument, this.escapeChar));
    }
}
