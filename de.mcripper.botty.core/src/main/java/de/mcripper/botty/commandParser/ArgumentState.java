package de.mcripper.botty.commandParser;

import de.mcripper.botty.commandParser.CommandParser.ParserState;

public class ArgumentState extends ParserState {

    private String argument = "";

    public ArgumentState(final CommandParser parser, final char firstChar) {
        super(parser);
        this.argument += firstChar;
    }

    @Override
    protected void parse(final char currentChar, final CommandBuilder builder) throws ParserException {
        if (!Character.isWhitespace(currentChar)) {
            this.argument += currentChar;
        } else {
            builder.addArg(this.argument);
            this.setState(new NeutralState(this.getParser()));
        }

    }

}
