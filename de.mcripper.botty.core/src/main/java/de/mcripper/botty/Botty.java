package de.mcripper.botty;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import de.mcripper.chatlib.Connection;
import de.mcripper.chatlib.xmpp.XmppModule;

/**
 * Main Entry Point for Botty.
 * 
 * @author Nik
 * 
 */
public class Botty {

    private final Connection connection;
    private final ShutdownHook showdownHook = new ShutdownHook() {

        @Override
        public void onShutdown() {
            Botty.this.connection.disconnect();
        }
    };
    private final CommandRegistry commandRegistry;

    @Inject
    private Botty(final Connection connection,
            final CommandRegistry commandRegistry,
            final BottyCommandProcessor commandProcessor,
            final Injector injector) {
        this.connection = connection;
        this.commandRegistry = commandRegistry;
        this.connection.addChatHandler(commandProcessor);
        ShutdownHooks.registerHook(this.showdownHook);
    }

    public static Botty create(final BottyConfiguration configuration) {
        final Injector injector = Guice.createInjector(new BottyModule(), new AbstractModule() {

            @Override
            protected void configure() {
                this.bind(BottyConfiguration.class).toInstance(configuration);
            }
        }, new XmppModule(configuration.getUser(), configuration.getPassword(), configuration.getServer(),
                configuration.getPort()));
        return injector.getInstance(Botty.class);
    }

    public void connect() {
        this.connection.connect();
    }

    public void disconnect() {
        this.connection.disconnect();
    }

    public CommandRegistry getCommandRegistry() {
        return this.commandRegistry;
    }
}
