package de.mcripper.botty;

import java.lang.reflect.Method;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Injector;

import de.mcripper.botty.commandParser.CommandInstance;
import de.mcripper.chatlib.Chat;

public class CommandRegistry {

    private final Logger logger = LoggerFactory.getLogger(CommandRegistry.class);
    private final Map<String, CommandInvoker> commands;
    private final Injector injector;

    @Inject
    public CommandRegistry(final Injector injector) {
        super();
        this.injector = injector;
        this.commands = Maps.newHashMap();
    }

    public void handleCommand(final CommandInstance cmd) {
        try {
            this.commands.get(cmd.getCommandName()).invoke(cmd);
        } catch (final Exception e) {
            throw new RuntimeException(String.format("Error while invoking command %s", cmd));
        }
    }

    public void registerCommand(final BottyCommand command) {

        this.injector.injectMembers(command);
        this.logger.debug("Registring commmand {}", command);
        for (final Method method : command.getClass().getMethods()) {
            final Command annotation = method.getAnnotation(Command.class);
            if (annotation != null && this.hasCommandInstanceParameter(method)) {
                this.logger.debug("Registering method {}", method);
                this.commands.put(annotation.name(), new CommandInvoker() {

                    @Override
                    public void invoke(final CommandInstance instance) throws Exception {
                        method.invoke(command, instance, instance.getOriginal().getChat());
                    }
                });
            }
        }
    }

    private boolean hasCommandInstanceParameter(final Method method) {
        final Class<?>[] types = method.getParameterTypes();
        return types.length == 2
                && CommandInstance.class.isAssignableFrom(types[0])
                && Chat.class.isAssignableFrom(types[1]);
    }

    private interface CommandInvoker {

        void invoke(CommandInstance command) throws Exception;
    }
}
