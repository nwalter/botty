package de.mcripper.botty;

import com.google.inject.Inject;

import de.mcripper.botty.commandParser.CommandInstance;
import de.mcripper.botty.commandParser.CommandParser;
import de.mcripper.botty.commandParser.ParserException;
import de.mcripper.chatlib.Chat;
import de.mcripper.chatlib.ChatHandler;
import de.mcripper.chatlib.Connection;
import de.mcripper.chatlib.Message;
import de.mcripper.chatlib.MessageHandler;

public class BottyCommandProcessor implements ChatHandler, MessageHandler {

    private final Connection connection;
    private final BottyConfiguration configuration;
    private final CommandRegistry commandRegistry;

    @Inject
    public BottyCommandProcessor(final Connection connection, final BottyConfiguration configuration,
            final CommandRegistry commandRegistry) {
        this.connection = connection;
        this.configuration = configuration;
        this.commandRegistry = commandRegistry;
        this.connection.addChatHandler(this);
    }

    @Override
    public void receive(final Chat chat, final Message message) {
        try {
            this.parseCommand(message);
        } catch (final ParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void parseCommand(final Message message) throws ParserException {
        final CommandInstance cmd = new CommandParser(this.configuration).parse(message);
        this.commandRegistry.handleCommand(cmd);
    }

    @Override
    public void chatCreated(final Chat chat) {
        chat.addMessageHandler(this);
    }

}
