package de.mcripper.botty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.BasicConfigurator;

public class Main {

    public static void main(final String[] args) throws IOException {
        BasicConfigurator.configure();

        final Botty botty = Botty.create(new PropertiesConfiguration());
        botty.getCommandRegistry().registerCommand(new HelpCommand());
        botty.connect();
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            final String command = bufferedReader.readLine();
            if (command.equals("exit")) {
                System.exit(0);
            }
        }
    }

}
