package de.mcripper.botty;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Singleton;

public class BottyModule extends AbstractModule implements Module {

    @Override
    protected void configure() {
        this.bind(CommandRegistry.class).in(Singleton.class);
    }

}
