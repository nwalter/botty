package de.mcripper.botty;

public interface BottyConfiguration {

    public String getServer();

    public String getPassword();

    public String getUser();

    public Integer getPort();

    public String getCommandPrefix();

}
