package de.mcripper.botty;

public interface ShutdownHook {

    void onShutdown();
}
