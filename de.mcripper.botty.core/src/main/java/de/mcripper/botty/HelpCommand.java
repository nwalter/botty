package de.mcripper.botty;

import de.mcripper.botty.commandParser.CommandInstance;
import de.mcripper.chatlib.Chat;

public class HelpCommand extends BottyCommand {

    @Command(name = "help")
    public void help(final CommandInstance instance, final Chat chat) {
        if (instance.getArguments().isEmpty()) {
            chat.sendMessage("Juhu, can I help you?");
        } else {
            chat.sendMessage(String.format("Sorry what do you mean with %s", instance.getArguments()));
        }
    }
}
