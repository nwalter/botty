package de.mcripper.botty.commandParser;

import org.junit.Assert;
import org.junit.Test;

public class CommandParserTest {

    @Test
    public void testSimpleCommand() throws ParserException {
        final CommandParser parser = new CommandParser(new StubConfiguration());
        final CommandBuilder builder = new CommandBuilder();
        parser.parse("!hallo", builder);

        Assert.assertEquals("hallo", builder.getCmd());
    }

    @Test
    public void testCommandWithArg() throws ParserException {
        final CommandParser parser = new CommandParser(new StubConfiguration());
        final CommandBuilder builder = new CommandBuilder();
        parser.parse("!hallo welt", builder);

        Assert.assertEquals("hallo", builder.getCmd());
        Assert.assertEquals(1, builder.getArgs().size());
        Assert.assertEquals("welt", builder.getArgs().get(0));
    }

    @Test
    public void testCommandWithEscArg() throws ParserException {
        final CommandParser parser = new CommandParser(new StubConfiguration());
        final CommandBuilder builder = new CommandBuilder();
        parser.parse("!hallo \"welt hier 'komme' ich\"", builder);

        Assert.assertEquals("hallo", builder.getCmd());
        Assert.assertEquals(1, builder.getArgs().size());
        Assert.assertEquals("welt hier 'komme' ich", builder.getArgs().get(0));
    }

    @Test
    public void testCommandWithMutlipleArgs() throws ParserException {
        final CommandParser parser = new CommandParser(new StubConfiguration());
        final CommandBuilder builder = new CommandBuilder();
        parser.parse("!hallo \"welt hier\n'komme'\" ich", builder);

        Assert.assertEquals("hallo", builder.getCmd());
        Assert.assertEquals(2, builder.getArgs().size());
        Assert.assertEquals("welt hier\n'komme'", builder.getArgs().get(0));
        Assert.assertEquals("ich", builder.getArgs().get(1));
    }

    @Test(expected = ParserException.class)
    public void testInvalidCommand() throws ParserException {
        final CommandParser parser = new CommandParser(new StubConfiguration());
        final CommandBuilder builder = new CommandBuilder();
        parser.parse("! hallo", builder);
    }

    @Test(expected = ParserException.class)
    public void testCommandWithIncompleteArg() throws ParserException {
        final CommandParser parser = new CommandParser(new StubConfiguration());
        final CommandBuilder builder = new CommandBuilder();
        parser.parse("!hallo \"welt hier 'komme' ich", builder);
    }

}
