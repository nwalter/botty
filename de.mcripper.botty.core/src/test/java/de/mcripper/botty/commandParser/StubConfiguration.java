package de.mcripper.botty.commandParser;

import de.mcripper.botty.BottyConfiguration;

public class StubConfiguration implements BottyConfiguration {
    @Override
    public String getUser() {
        return "Botty";
    }

    @Override
    public String getServer() {
        return "test";
    }

    @Override
    public Integer getPort() {
        return 8080;
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public String getCommandPrefix() {
        return "!";
    }
}