Botty - the XMPP bot
====================

What is Botty? 
--------------
Botty is a XMPP chat bot written in Java.
It depends on an modular architecture, so it is flexible and easy to extend.
Botty is able to participate in multiuserchats.

What is Botty build of?
-----------------------
Botty is entirely written in Java.
It uses the following libraries:

-   Maven for build and dependency management
-   Google Guava
-   Google Guice for DI
-   IgniteRealtimes Smack for the XMPP Communication Layer
-   SLF4J for Logging

How is Botty licensed? 
----------------------
Botty is licensed under [GPLv3][].

[GPLv3]: http://www.gnu.org/licenses/gpl